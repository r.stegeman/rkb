# Changelog


## [1.3] - 2020-02-01

### Added
- `ssh` command to `.bashrc`

### Changed
- Structure small overhaul
- .local use and documentation

## [1.2] - 2020-03-31

### Added

- AI knowledge file
- Changelog file

## [1.1] - 2020-02-29 to 2020-03-30

### Added

- Dockerfiles and registry
- Knowledge folder
  - python
  - ubuntu
  - vscode
  - vue
- Scripts folder
  - docker
  - python
    - `env.py`
    - `flaskApp.py`
    - `logger.py`
- Notes folder
- Shortcuts folder
  - windows
- Config folder


## [1.0] - 2020-02-29

### Added

- Start of RKB
